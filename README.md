# Shared Ansible roles

This repository contains DevOps group shared ansible roles. It can be paired
with the [Ansible role fetcher
utility](https://gitlab.developers.cam.ac.uk/uis/devops/tools/ansibleroles) to
provide shared roles for Ansible playbooks.

## Best Practices

All roles in this repository should:

- Have a README
- Have a usage example
- Assert all required variables are set
- Be idempotent: running the role twice should have the same effect as running
    it once.
- Be compose-able: roles should configure one small thing so that they may be
    combined together to solve bigger problems. Small roles should support being
    include-ed within larger roles.
- Be orthogonal: roles should not replace files likely to be touched by other
    roles.
- Should pass ansible-lint checking

Roles in this repo must be general purpose with all settings that can be set to
a sensible default put in the defaults file.

## Variables

Any settings where a default makes no sense must assert the variable is set.

## READMEs

Every role in here must have a `README.md` based on the `template-role-README.md`

## Check mode

Roles here must be able to be run in check mode on an configured system

## Changed

Roles here must be able to be run on a configured system without returning any
`changed` states. That is to say: when running the playbook twice, the first run
will configure the system the second will find everything working correctly and
make no changes.

## Lint

Every role in here will be automatically linted by ansible-lint and must pass
before being accepted here
Role `docker-swarm` is excluded, as for now it has multiple issues with a new
version of `ansible-lint`.

## Automatic testing

> ***TODO*** Every role here will be automatically tested on Ubuntu 16.04, 18.04
> and CentOS 7 and must assert if it can't be run on one of these
