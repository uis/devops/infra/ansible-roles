# Role: configure_bes
This role connects to bes and templates in a preseed file for installing an OS

## Required vars

- **bes_keyboard_access_user**: The fluffy username for emergency keyboard access eg "DevOps User".
- **bes_keyboard_access_username**: The username for emergency keyboard access login eg "devops".
- **bes_keyboard_access_user_crypted_password**: A UNIX crypted password for emergency keyboard access. Probably best to be a password you can type by hand on a crummy terminal (think VMWare remote console).
- **ssh_key_users**: The list of users whom ssh keys should be included.

## Required vars with defaults

 - **bes_path_to_pub_keys**. Default ```/usr/share/ansible/roles/ssh/files/```. Location (in the local machine executing ansible) of the public keys to use.
 - **bes_preseed_file_name**. Default ```preseed-1disk-no-raid.cfg```.  Preseed file to use, currenlty in templates folder of the configure-bes role.
 - **bes_install_version**. Default ```bionic```. Ubuntu version to use in the preseed file.
 - **bes_ntp_servers**. Default: ```['ntp0.csx.cam.ac.uk', 'ntp1.csx.cam.ac.uk', 'ntp2.csx.cam.ac.uk']```. NTP Servers to use.
 - **bes_grub_dev**. Default: ```/dev/sda```. Location of GRUB.
 - **bes_install_repo_server**. Default: ```www-uxsup.csx.cam.ac.uk```. APT repo to use.
 - **bes_install_repo_dir**. Default: ```/pub/linux/ubuntu```. Directory inside the APT repo to use.

## Running
This role needs to be run as a user that has access to bes to change the install files for the hosts being worked on.
eg
```yaml
- hosts: <some hosts>
  user: "{{ lookup('env','BES_USER') }}"
  gather_facts: false
  roles:
    - configure_bes
```
