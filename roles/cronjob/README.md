# Role: cronjob
This role installs a service (by default) using a
[crazymax/swarm-cronjob](https://github.com/crazy-max/swarm-cronjob) image.
A container using this image acts like a crond service, causing other
containers to be started at scheduled times.

## Variables

No variables are required, though the following may be changed. Actually,
cronjob task configuration is placed in labels on other containers.

- `cronjob_loglevel`

  What level of logging cronjob image outputs at (default: **info**)

- `cronjob_logjson`

  Whether cronjob image logging is in json format (default: **false**)

- `cronjob_tz`

  Timezone assigned to the scheduler (default: **UTC**)

## Running

In addition to using this shared role in a playbook, additional
`docker_swarm_service` ansible tasks need to created for the scheduled actions.
These services must have `replicas: 0` to stop them automatically starting and
`restart_policy: none` to stop them restarting after being triggered.

**/playbook.yml**
```yaml
- hosts: <some hosts>
  roles:
    - cronjob
```

**/roles/{service}/tasks/main.yml** (for example)
```yaml
- name: scheduled service
  run_once: true
  docker_swarm_service:
    name: scheduled-action
    image: "{{ service_image }}"  # image containing action to perform

    restart_policy: none          # Prevent restarts      - CRITICAL
    replicas: 0                   # Prevent initial start - CRITICAL

    # Override image's CMD with list (command and arguments), if necessary
    # (ansible's docker_swarm_service.args => docker-compose's service.command)
    args:
      - "/bin/sh"
      - "-c"
      - 'echo "$(date) - triggered" && sleep 1m && echo "$(date) - done"'

    # Labels to be used by cronjob container
    labels:                       
      swarm.cronjob.enable: "true"
      swarm.cronjob.schedule: "0 30 * * * *"  # cron-like schedule [1]
      swarm.cronjob.skip-running: "true"       # don't start if already running
```

[1] - **Warning:** the schedule format is described [here](https://godoc.org/github.com/crazy-max/cron#hdr-CRON_Expression_Format) and includes seconds at the beginning. e.g. 0 30 * * * * is half past every hour
