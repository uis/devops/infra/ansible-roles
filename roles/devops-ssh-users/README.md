# Role: add-devops-users
This role adds user accounts for members of the DevOps group.

## Required vars

- devops_users

  list of users and keys to add (in correct format for gp add_sudo_users role, see [add_sudo_users](https://gitlab.developers.cam.ac.uk/uis/devops/infra/ansible/infra/tree/devops/gp_roles/add_sudo_users))

  ```See defaults/main.yml```

## Optional vars

- ssh_revoked_keys

  Keys to be removed (and for which user)

## Dependencies
This role depends upon the add_sudo_users gp_roles.

## Running
```yaml
- hosts: <some hosts>
  roles:
    - devops-ssh-users
```
