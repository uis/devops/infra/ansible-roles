# Role: docker-ce

This role installs and configures Docker CE. 

## Required vars

No required vars

## Running

This role can be run using the following example playbook:

```yaml
- hosts: <some hosts>
  become: true
  roles:
    - docker-ce
```

## Docker GCloud Logging

It is possible to optionally configure Docker to log to GCloud. To enable this,
set `docker_gcloud_logging` to `true` and provide `gcloud_logger_credentials` as 
appropriate.
