# Role: docker-login

This role configures Docker CE with credentials for a repository - thus allowing it to 
access that repositories private resources.

## Role Variables

`docker_login_registry_credentials`

A dictionary of credentials for all connected repositories (default: {}). 
An example entry might be:

```
docker_login_registry_credentials:
  the_gitlab_repo:
    username: !vault..
    password: !vault..
    registry: registry.gitlab.developers.cam.ac.uk
```

## Running

This role can be run using the following example playbook:

```yaml
- hosts: <some hosts>
  become: true
  roles:
    - docker-login
```
