# Role: portainer 
This role deploys portainer, a web UI to manage and maintain Docker environments.

## Dependencies
- traefik

## Required vars 
- portainer_admin_password     
  ```The admin login password``` 
  
- portainer_dashboard_host  
  ```URL for the portainer dashboard```  
  eg:  
  ```test-portainer.infra.lc.gcloud.automation.uis.cam.ac.uk```

## Role Variables

Other optional variables can be defined and are described in the role's [defaults file](defaults/main.yml)

## Running

```yaml
- hosts: <some hosts>
  user: <foo|omit>
  become: <true|omit>
  roles:
    - role: portainer
      vars:
        portainer_dashboard_host: "https://portainer.invalid.cam.ac.uk"
        portainer_admin_password: !vault | ..
```
