# Add the portainer swarm management console to the swarm
#
# Tasks in this file are all run with run_once: true which is inherited from
# main.yml
---

# Create a network for all portainer agents. Only containers on this network
# will be able to talk to agents.

- name: Portainer agent docker network
  community.docker.docker_network:
    name: "{{ portainer_agent_network_name }}"
    driver: overlay

# The portainer admin passowrd is stored in a docker secret. If this ever
# changes, one needs ot manually remove the portainer service first.

- name: Portainer admin password secret
  community.docker.docker_secret:
    name: portainer-crypted-password
    data: "{{ portainer_admin_password }}"
  register: portainer_pass

# The portainer manager and agent services. We can't call this service
# "portainer" because of https://github.com/ansible/ansible/issues/50654

- name: Deploy portainer service
  community.docker.docker_swarm_service:
    name: portainer-ui
    constraints:
      - "node.role==manager"
    mounts:
      - source: /var/run/docker.sock
        target: /var/run/docker.sock
        type: bind
    networks:
      - "{{ traefik_network_name }}"
      - "{{ portainer_agent_network_name }}"
    image: "{{ portainer_image }}"
    args:
      - "-H"
      - "tcp://tasks.portainer-agent:9001"
      - "--tlsskipverify"
      - "--admin-password-file"
      - "/run/secrets/portainer-pass"
    secrets:
      - secret_id: "{{ portainer_pass.secret_id }}"
        secret_name: portainer-crypted-password
        filename: "/run/secrets/portainer-pass"
    labels:
      traefik.port: "9000"
      traefik.docker.network: "{{ traefik_network_name }}"
      traefik.frontend.rule: "Host:{{ portainer_dashboard_host }}"
      traefik.frontend.headers.SSLRedirect: "true"

    restart_policy: any
    restart_policy_attempts: 5
    restart_policy_window: 30

    replicas: 1

    # The portainer image doesn't have /etc/passwd so bare UIDs are necessary
    user: "0"

- name: Deploy portainer agent service
  community.docker.docker_swarm_service:
    name: portainer-agent
    constraints:
      - "node.role==manager"
    mounts:
      - source: /var/run/docker.sock
        target: /var/run/docker.sock
        type: bind
      - source: /var/lib/docker/volumes
        target: /var/lib/docker/volumes
        type: bind
    mode: global
    networks:
      - "{{ traefik_network_name }}"
      - "{{ portainer_agent_network_name }}"
    image: "{{ portainer_agent_image }}"
    env:
      - AGENT_CLUSTER_ADDR=tasks.portainer-agent

    restart_policy: any
    restart_policy_attempts: 5
    restart_policy_window: 30

    replicas: 1

    # The portainer image doesn't have /etc/passwd so bare UIDs are necessary
    user: "0"
