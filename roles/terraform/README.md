# Role: terraform

This role sets the fact `terraform_outputs` from json contained in the `TERRAFORM_OUTPUTS` environment variable.

This is usually set by the run-ansible-playbook.sh using:

`export TERRAFORM_OUTPUTS="$( cd gcp && terraform output -json )"`

This role will always set the facts irrespective of which tags are selected.

## Required vars

- `TERRAFORM_OUTPUTS` (environment variable)

 JSON output from terraform output -json

## Running

Typically wants to be included for all hostvars, eg
```yaml
- hosts: all
  gather_facts: false
  roles:
    - terraform
```
