# Role: traefik

This role installs the traefik service to a docker swarm cluster. Traefik is a modern HTTP reverse
proxy and load balancer. Traefik integrates with docker swarm cluster and configures itself 
automatically and dynamically.

## Dependencies

- `docker-swarm`
- `zookeeper`

## Role Variables

The following 2 variable should be configured if you wish to configure the traefik dashboard proxy
service for the purposes of authentication.

- `traefik_dashboard_lookup_group_id`
- `traefik_dashboard_proxy_cookie_key`

It is recommended that you configure this proxy.

## Running
Any special settings or constraints needed to run this role
eg
```yaml
- hosts: <some hosts>
  become: true
  roles:
    - role: traefik
      vars:
        traefik_dashboard_host: "traefik.infra.<some project>.gcloud.automation.uis.cam.ac.uk"
```
