# Role: zookeeper

This role installs the apache zookeeper service to a docker swarm cluster. Provides data synchronisation services to other
services distributed on the swarm cluster (eg. traefik).

## Dependencies

* `docker-swarm`

## Required vars

- `zookeeper_root`

A path to the root where zookeeper stores it's data.

## Role Variables

Other optional variables can be defined and are described in the role's [defaults file](defaults/main.yml)

## Running
Any special settings or constraints needed to run this role
eg
```yaml
- hosts: <some hosts>
  become: true
  roles:
    - role: zookeeper
      vars:
        zookeeper_root: "/var/lib/zookeeper"
```
