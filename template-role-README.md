# Role: `name_of_role`
`brief description of role and purpose`

## Dependencies
* `Role this role depends on`
* `Another role this depends on`

## Required vars

- a_required_var_for_this_role

  ```Describe what this var is and a what it should be set to```

### Optional vars
See the defaults folder for variables that can be overridden to suit your environment

## Running
Any special settings or constraints needed to run this role
eg
```yaml
- hosts: <some hosts>
  user: <foo|omit>
  become: <true|omit>
  roles:
    - name_of_role
```
